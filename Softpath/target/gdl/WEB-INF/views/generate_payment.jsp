<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pagos Softpath</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script type="text/javascript">

	function clean()
	{
		document.getElementById("amountlabel").innerHTML = "";
		document.getElementById("payment_datelabel").innerHTML = "";
	}

	function validar() {
		
		clean();

		if (document.pagoform.amount.value == "") {
			document.getElementById("amountlabel").innerHTML = "La cantidad es necesaria para generar nuevo pago";
			document.pagoform.amount.focus();
			return false;
		}

		if (document.pagoform.payment_date.value == "") {
			document.getElementById("payment_datelabel").innerHTML = "La fecha no puede estar vacia";
			document.pagoform.payment_date.focus();
			return false;
		}

		return true;

	}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75794966-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>

	<h2>
		Crear nueva order de pago para <label>${programmer.name}
			${programmer.lastname}</label>
	</h2>
	<form action="doPaymentl" name="pagoform">
		<input type="hidden" name="id" value="${programmer.id}" />
		<table>
			<tr>
				<td>Pago numero:</td>
				<td><label>${num_pago+1}</label></td>
			</tr>
			<tr>
				<td>Cantidad a Pagar</td>
				<td><input id="amount" type="number" name="amount" /></td>
				<td><label id="amountlabel"></label></td>
			</tr>
			<tr>
				<td>Fecha:</td>
				<td><input type="date" name="payment_date" /></td>
				<td><label id="payment_datelabel"></label></td>
			</tr>
			<tr>
				<td><input type="submit" value="Generar nuevo pago" onclick="return(validar());"></td>
			</tr>
		</table>
	</form>
	
	

	<div class="row">
	 <div class="col-xs-12 col-md-8">
	 <h2>Desgloce de pagos</h2>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<td>Numero de pago</td>
				<td>Fecha de Pago</td>
				<td>Cantidad</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${payments}" var="payment" varStatus="status">
				<tr>
					<td>${status.count}</td>
					<td>${payment[1]}</td>
					<td>${payment[0]}</td>
				</tr>
				<c:set var="suma" value="${suma=payment[0]+suma}"></c:set>
			</c:forEach>
		</tbody>
		<tr>
			<td></td>
			<td><strong>Total: </strong></td>
			<td><strong>${suma}</strong></td>
		</tr>
	</table>
	</div></div>
	<div class="row">
	 <div class="col-md-4 col-md-offset-1">
	<h3>
		<a href="<c:url value='admin' />">   Go back to Programmers Page</a>
	</h3></div>
	</div>
</body>
</html>