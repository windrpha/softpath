<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>

<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<c:url value="/resources/images/icons/apple-icon-57x57.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/images/icons/apple-icon-114x114.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/images/icons/apple-icon-72x72.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/images/icons/apple-touch-icon-144x144.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<c:url value="/resources/images/icons/apple-touch-icon-60x60.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<c:url value="/resources/images/icons/apple-touch-icon-120x120.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<c:url value="/resources/images/icons/apple-touch-icon-76x76.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<c:url value="/resources/images/icons/apple-touch-icon-152x152.png" />" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-196x196.png" />" sizes="196x196" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-96x96.png" />" sizes="96x96" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-32x32.png" />"  sizes="32x32" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-16x16.png" />" sizes="16x16" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-128.png" />"  sizes="128x128" />


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
<link href="<c:url value="/resources/css/login.css" />" rel="stylesheet">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75794966-1', 'auto');
  ga('send', 'pageview');

</script>

<title>Login Softpath</title>
</head>
<body>
	<div class="container">
		<section id="content">
			<c:url var="loginUrl" value="login" />
			<form action="${loginUrl}" method="post">
				<h1>SoftPath</h1>
				<div>
					<input type="text" placeholder="Username" id="username" name="username" />
				</div>
				<div>
					<input type="password" placeholder="Password" id="password" name="password"/>
				</div>
				<input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
				<div>
					<input type="submit" value="Log in" />
				</div>
			</form>
		</section>
	</div>
	<!-- container -->
</body>
</html>