package com.softpath.service;

import java.util.List;

import com.softpath.entity.Payments;
import com.softpath.entity.User;

public interface UserDao {

	
	public User loadUserById(String username);
	public List<User> getAllProgramers();
	public void createProgrammer(User programmer);
	public boolean checkIfeExist(String username);
	public User getUserById(long id);
	public void deleteById(long id);
	public String getNamebyUsername(String username);
	public int getPaymentNumber(long id);
	public String getFullNamebyUsername(String username);
	public void savePayment(Payments payment, long id);
	public List<Object[]> getPaymentsById(long id);
	public long getIdByUsername(String username);
	public double getTotalAmount(long id);
	public void saveProgramer(User user);
	public void updateProgramer(User user);
	
}
