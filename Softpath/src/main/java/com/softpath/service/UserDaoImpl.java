package com.softpath.service;

import java.math.BigInteger;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.softpath.entity.Payments;
import com.softpath.entity.User;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User loadUserById(String username) {
	
	 Query query = sessionFactory.getCurrentSession().createQuery("from User where username=:myid");
	 query.setParameter("myid", username);
	 User user = (User)query.uniqueResult();
	 return user;
	}

	@Override
	public List<User> getAllProgramers() {
		Query query = sessionFactory.getCurrentSession().createQuery("from User where userRol=:therol");
		query.setParameter("therol","USER");
		List<User> list = query.list();
		return list;
	}

	@Override
	public void createProgrammer(User programmer) {
	
		sessionFactory.getCurrentSession().saveOrUpdate(programmer);
		
	}

	@Override
	public boolean checkIfeExist(String username) {
		Query query = sessionFactory.getCurrentSession().createQuery("from User where username=:usern");
		query.setParameter("usern", username);
		@SuppressWarnings("unchecked")
		List<User> list = query.list();
		return list.size() > 0 ? true:false;
	}

	@Override
	public User getUserById(long id) {
		 Query query = sessionFactory.getCurrentSession().createQuery("from User where id=:myid");
		 query.setParameter("myid", id);
		 User user = (User)query.uniqueResult();
		 return user;
	}

	@Override
	public void deleteById(long id) {
		Query query = sessionFactory.getCurrentSession().createQuery("delete from User where id=:idd");
		query.setParameter("idd", id);
		query.executeUpdate();
	}

	@Override
	public String getNamebyUsername(String username) {
		Query query = sessionFactory.getCurrentSession().createQuery("select name from User where username = ?");
		query.setParameter(0,username);
		String name = (String) query.uniqueResult();
		return name;
	}

	@Override
	public int getPaymentNumber(long id) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select count(*) from PAGOS where programer_id =:myid");
		query.setParameter("myid", id);
		BigInteger res = (BigInteger)query.uniqueResult();
		int res1 = res.intValue();
		return res1;
	}

	@Override
	public void savePayment(Payments payment, long id) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"insert into PAGOS(amount,payment_date,programer_id) values(:amount,:payment,:programer)");
		query.setParameter("amount", payment.getAmount());
		query.setParameter("payment", payment.getPayment_date());
		query.setParameter("programer",id);
		query.executeUpdate();
		Query query1 = sessionFactory.getCurrentSession().createQuery("from User where id=:myid");
		query1.setParameter("myid", id);
		User programer = (User)query1.uniqueResult();
		programer.setAmount_to_pay(programer.getAmount_to_pay() - payment.getAmount());
		sessionFactory.getCurrentSession().saveOrUpdate(programer);
		
	}

	@Override
	public List<Object[]> getPaymentsById(long id) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select amount,payment_date from PAGOS where programer_id =:myid");
		query.setParameter("myid", id);
		List<Object[]> list = (List<Object[]>)query.list();
		return list;
	}

	@Override
	public long getIdByUsername(String username) {
		Query query = sessionFactory.getCurrentSession().createQuery("select id from User where username=:myusername");
		query.setParameter("myusername", username);
		return (long)query.uniqueResult();
	}

	@Override
	public double getTotalAmount(long id) {
		Query query = sessionFactory.getCurrentSession().createQuery("select amount_to_pay from User where id=:myid");
		query.setParameter("myid",id);
		return (double)query.uniqueResult();
	}

	@Override
	public String getFullNamebyUsername(String username) {
		Query query = sessionFactory.getCurrentSession().createQuery("select name, lastname from User where username=:myusername");
		query.setParameter("myusername", username);
		@SuppressWarnings("unchecked")
		List<Object[]> lista = (List<Object[]>)query.list();
		String name="";
		String lastname="";
		for(Object[] list : lista)
		{
			name = list[0].toString();
			lastname = list[1].toString();
		}
		return name + " " + lastname;
	}

	@Override
	public void saveProgramer(User user) {
		sessionFactory.getCurrentSession().save(user);
		
	}

	@Override
	public void updateProgramer(User user) {
		
		sessionFactory.getCurrentSession().update(user);
		
//		Query query = sessionFactory.getCurrentSession().createSQLQuery("update USERS set age = :myage, company=:mycompany"
//				+ ",date_hired=:mydate, email=:myemail,groupNum=:mygroup,lastname=:mylastname,name=:myname,"
//				+ "password=:mypassword,phone=:myphone,place=:myplace,salary=:mysalary,softpath_email=:mysemail,"
//				+ "softpath_phone=:mysphone where id =:myid");
//		query.setParameter("myage",user.getAge());
//		query.setParameter("mycompany",user.getCompany());
//		query.setParameter("myemail",user.getEmail());
//		query.setParameter("mygroup",user.getGroupNum());
//		query.setParameter("mylastname",user.getLastname());
//		query.setParameter("mypassword",user.getPassword());
//		query.setParameter("myphone",user.getPhone());
//		query.setParameter("myplace",user.getPlace());
//		query.setParameter("mysalary",user.getSalary());
//		query.setParameter("mysemail",user.getSoftpath_email());
//		query.setParameter("mysphone",user.getSoftpath_phone());
//		query.executeUpdate();		
	} 
	
	
	
	
}
