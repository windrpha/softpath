package com.softpath.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.softpath.entity.User;
import com.softpath.service.UserDao;

public class ValidatorUser implements Validator {
	
	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors error) {
	
		User user = (User)target;
		if(user.getAmount_to_pay() < 29000)
		{
			error.rejectValue("amount_to_pay", "error.amount");
		}
		
	}
}
