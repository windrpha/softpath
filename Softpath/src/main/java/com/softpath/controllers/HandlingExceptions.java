package com.softpath.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandlingExceptions {

	
	@ExceptionHandler(Exception.class)
	public String exception(Exception e) {

		return "home";
	}
	
	
}
