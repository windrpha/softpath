package com.softpath.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.softpath.entity.Payments;
import com.softpath.entity.User;
import com.softpath.service.UserDao;
import com.softpath.validation.ValidatorUser;

@Controller
public class HomeController {

	@Autowired
	private UserDao dao;

	@ModelAttribute("programmer")
	public User getProgrammer() {
		return new User();
	}

	@RequestMapping(value = { "/", "/home" }, method = { RequestMethod.GET, RequestMethod.POST })
	public String getHome() {
		return "home";
	}

	@RequestMapping(value = "/login", method = { RequestMethod.GET, RequestMethod.POST })
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/admin", method = { RequestMethod.GET, RequestMethod.POST })
	public String admin(ModelMap model) {
		model.addAttribute("programmers", dao.getAllProgramers());
		model.addAttribute("user", dao.getNamebyUsername(getPrincipal()));
		return "admin";
	}

	@RequestMapping(value = "/programador", method = { RequestMethod.GET, RequestMethod.POST })
	public String programmer(ModelMap map) {
		map.addAttribute("programmer_name", dao.getNamebyUsername(getPrincipal()));
		long id = dao.getIdByUsername(getPrincipal());
		List<Object[]> list = dao.getPaymentsById(id);
		map.addAttribute("amount_to_pay", dao.getTotalAmount(id));
		map.addAttribute("payments", list);
		return "programmer_page";
	}

	@RequestMapping(value = "/accessdenied", method = { RequestMethod.GET, RequestMethod.POST })
	public String denied() {
		return "deniedPage";
	}

	@RequestMapping(value = "/crear", method = { RequestMethod.GET, RequestMethod.POST })
	public String createProgramer(ModelMap model) {
		model.addAttribute("create", true);
		return "create";
	}

	@RequestMapping(value = "/create_programmer", method = RequestMethod.POST)
	public String submitProgramer(@Valid @ModelAttribute("programmer") User programmer, BindingResult result,
			ModelMap model) {
			programmer.setUserRol("USER");
			ValidatorUser validator = new ValidatorUser();
			validator.validate(programmer, result);
			
			boolean exist = dao.checkIfeExist(programmer.getUsername());
			if(exist)
				result.rejectValue("username", "error.usernameifexist");
			
		if (result.hasErrors()) {
			System.out.println(result.getAllErrors());
			return "create";
		}		


		dao.saveProgramer(programmer);
		return "redirect";
	}
	
	@RequestMapping(value="/update_programmer", method = RequestMethod.POST)
	public String updateProgrammer(@ModelAttribute("programmer") User programmer)
	{
		
		dao.updateProgramer(programmer);
		return "redirect";
	}
	

	@RequestMapping(value = "/delete-{id}", method = { RequestMethod.GET, RequestMethod.POST })
	public String deleteProgrammer(@PathVariable long id) {
		dao.deleteById(id);
		return "redirect";

	}

	@RequestMapping(value = "/update-{id}", method = { RequestMethod.GET, RequestMethod.POST })
	public String updateProgrammer(@PathVariable long id, ModelMap model) {
		User user = dao.getUserById(id);
		model.addAttribute("programmer_model", user);
		model.addAttribute("create", false);
		return "update";

	}

	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/home?logout";
	}

	@RequestMapping(value = "/pagar-{id}", method = { RequestMethod.GET, RequestMethod.POST })
	public String generatePayment(@PathVariable long id, ModelMap model) {
		User programmer = dao.getUserById(id);
		model.addAttribute("programmer", programmer);
		model.addAttribute("num_pago", dao.getPaymentNumber(id));
		List<Object[]> list = dao.getPaymentsById(id);
		model.addAttribute("payments", list);
		return "generate_payment";

	}

	@RequestMapping(value = "/doPaymentl", method = { RequestMethod.GET, RequestMethod.POST })
	public String createPayments(@RequestParam("id") String id, @RequestParam("amount") String amount,
			@RequestParam("payment_date") String payment_date) {
		Payments pay = new Payments();
		pay.setAmount(Double.parseDouble(amount));
		pay.setPayment_date(payment_date);
		dao.savePayment(pay, Long.parseLong(id));
		return "redirect";
	}

}
