package com.softpath.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({"com.softpath.configuration"})
@PropertySource(value={"classpath:application.properties"})
public class HibernateConfiguration {

	@Autowired
	private Environment environment;
	
	@Bean
	public DataSource dataSource()
	{

		DriverManagerDataSource dataSource = new DriverManagerDataSource();

		/* THIS IS THE CONEXION FOR PROD DATABASE IN www.softpathtec.com */
		
//		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
//		dataSource.setUrl("jdbc:mysql://mysql3000.mochahost.com/rpp8906_softpath_prod");
//		dataSource.setUsername("rpp8906_softpath");
//		dataSource.setPassword("leafar1520");
		
		/* THIS IS THE CONECTION FOR TEST DATABASE */
		

		dataSource.setDriverClassName(environment.getRequiredProperty("hibernate.driverClassName"));
		dataSource.setUrl(environment.getRequiredProperty("hibernate.url"));
		dataSource.setUsername(environment.getRequiredProperty("hibernate.username"));
		dataSource.setPassword(environment.getRequiredProperty("hibernate.password"));
		
		return dataSource;
	}
	
	@Bean
	public LocalSessionFactoryBean sessionFactory()
	{
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[]{"com.softpath.entity","com.softpath.security"});
		sessionFactory.setHibernateProperties(myProps());
		return sessionFactory;
	}
	
	private Properties myProps()
	{
		Properties pro = new Properties();
		pro.put("hibernate.dialect","org.hibernate.dialect.MySQLDialect");
		pro.put("hibernate.show_sql","true");
		pro.put("hibernate.hbm2ddl.auto", "update");
		pro.put("hibernate.format_sql", "true");
		return pro;
	}
	
	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory s)
	{
		HibernateTransactionManager manager = new HibernateTransactionManager();
		manager.setSessionFactory(s);
		return manager;
	}
	
}

