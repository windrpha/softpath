package com.softpath.entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="USERS")
public class User {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String username;
	@NotEmpty(message="El programador necesita un nombre")
	private String name;
	@NotEmpty(message="El programador necesita un apellido")
	private String lastname;
	@NotEmpty(message="El programador necesita una contraseña")
	private String password;
	@NotNull(message="La edad del programador no puede estar vacia")
	private int age;
	private String company;
	@NotEmpty(message="El programador debe estar en algun grupo")
	private String groupNum;
	private String date_hired;
	private double salary;
	private String place;
	private double amount_to_pay;
	@Email@NotEmpty(message="El mail personal no puede estar vacio")
	private String email;
	@NotEmpty(message="El telefono personal no puede estar vacio")
	private String phone;
	@Email
	private String softpath_email;
	private String softpath_phone;
	@OneToMany(mappedBy="programer")
	private Collection<Payments> payments = new ArrayList<Payments>();
	@NotEmpty(message="Selecciona algun rol")
	private String userRol;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getSoftpath_email() {
		return softpath_email;
	}
	public void setSoftpath_email(String softpath_email) {
		this.softpath_email = softpath_email;
	}
	public String getSoftpath_phone() {
		return softpath_phone;
	}
	public void setSoftpath_phone(String softpath_phone) {
		this.softpath_phone = softpath_phone;
	}
	
	
	public String getUserRol() {
		return userRol;
	}
	public void setUserRol(String userRol) {
		this.userRol = userRol;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getGroupNum() {
		return groupNum;
	}
	public void setGroupNum(String groupNum) {
		this.groupNum = groupNum;
	}
	
	public String getDate_hired() {
		return date_hired;
	}
	public void setDate_hired(String date_hired) {
		this.date_hired = date_hired;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public double getAmount_to_pay() {
		return amount_to_pay;
	}
	public void setAmount_to_pay(double amount_to_pay) {
		this.amount_to_pay = amount_to_pay;
	}
	public Collection<Payments> getPayments() {
		return payments;
	}
	public void setPayments(Collection<Payments> payments) {
		this.payments = payments;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	
	
}
