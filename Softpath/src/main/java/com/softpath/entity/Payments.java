package com.softpath.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PAGOS")
public class Payments {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private double amount;
	private String payment_date;
	@ManyToOne
	private User programer;
	
	
	
	public User getProgramer() {
		return programer;
	}
	public void setProgramer(User programer) {
		this.programer = programer;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getPayment_date() {
		return payment_date;
	}
	public void setPayment_date(String payment_date) {
		this.payment_date = payment_date;
	}
	
	
}
