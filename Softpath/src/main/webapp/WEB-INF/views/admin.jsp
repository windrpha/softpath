<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<title>Administrador Softpath</title>
<script type="text/javascript">
	window.onload = function() {

		var a = document.getElementById("deletelink");

		a.onclick = function() {

			if (confirm("¿Quieres realmente borrar al programador?") == true)
				return true;
			else
				return false;
		}
	}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75794966-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>



	<h2>Programadores SoftPath</h2>
	<div class="row">
		 <div class="col-md-4">
		Dear <strong>${user}</strong>, Welcome to Admin Page. </div>
		<div class="col-md-4 col-md-offset-4" >
		<a href="<c:url value="/logout" />">Logout</a><span> |</span> 
		<a href="<c:url value="/" />">Home</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover table-bordered">
				<thead>
					<tr class="success">
						<td>Nombre</td>
					<!--  
						<td>Apellidos</td>
						<td>Edad</td>
						<td>Phone</td>
						<td>Phone SoftPath</td>
						<td>Mail</td>
						<td>Mail SoftPath</td>
						-->
						<td>Username</td>
						<td>Password</td>
						<!--  
						<td>Empresa</td>
						<td>Grupo</td>
						<td>Fecha Contratación</td>
						<td>Lugar Trabajo</td>
						-->
						<td>Salario</td>
						<td>Debe</td>
						<td><strong>Generar Pago</strong></td>
						<td>Actualizar</td>
						<!-- 		<td>Borrar</td> -->
					</tr>
				</thead>
				<c:forEach items="${programmers}" var="programmer">
					<tr>
						<td>${programmer.name}</td>
						<!--  
						<td>${programmer.lastname}</td>
						<td>${programmer.age}</td>
						<td>${programmer.phone}</td>
						<td>${programmer.softpath_phone}</td>
						<td>${programmer.email}</td>
						<td>${programmer.softpath_email}</td>
						-->
						<td>${programmer.username}</td>
						<td>${programmer.password}</td>
						<!--  
						<td>${programmer.company}</td>
						<td>${programmer.groupNum}</td>
						<td>${programmer.date_hired}</td>
						<td>${programmer.place}</td>
						-->
						<td>${programmer.salary}</td>
						<td>${programmer.amount_to_pay}</td>
						<td><a href="<c:url value='/pagar-${programmer.id}'/>">Nuevo
								Pago</a></td>
						<td><a href="<c:url value='/update-${programmer.id}'/>">Editar</a></td>
						<!-- 		<td><a id="deletelink"
					href="<c:url value='/delete-${programmer.id}'/>">Delete</a></td> -->
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<h3>
		<a href="<c:url value='/crear' />">Crear nuevo programador</a>
	</h3>
</body>
</html>