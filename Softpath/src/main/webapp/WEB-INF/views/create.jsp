<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<title>Insert title here</title>
<style type="text/css">
.error {
	color: #ff0000;
}

</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75794966-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>
			<h2>Lets create another programmer!</h2>
			<h3>
				<a href="<c:url value='admin' />">Go back to Programmers
					Page</a>
			</h3>
	
	<form:form action="create_programmer" method="POST"
		modelAttribute="programmer">
		<form:input type="hidden" path="id" value="${programmer_model.id}" />
		<form:input type="hidden" path="userRol" value="USER" />
		<table class="table-responsive table-condensed">
			<tr>
				<td>Nombre:</td>
				<td><form:input path="name" value="${programmer_model.name}" /></td>
				<td><form:errors path="name" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Apellido:</td>
				<td><form:input path="lastname"
						value="${programmer_model.lastname}" /></td>
				<td><form:errors path="lastname" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Edad:</td>
				<td><form:input path="age" value="${programmer_model.age}" /></td>
				<td><form:errors path="age" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Email Personal:</td>
				<td><form:input path="email" value="${programmer_model.email}" /></td>
				<td><form:errors path="email" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Email SoftPath:</td>
				<td><form:input path="softpath_email"
						value="${programmer_model.softpath_email}" /></td>
				<td><form:errors path="softpath_email" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Telefono Personal:</td>
				<td><form:input path="phone" value="${programmer_model.phone}" /></td>
				<td><form:errors path="phone" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Telefono SoftPath:</td>
				<td><form:input path="softpath_phone"
						value="${programmer_model.softpath_phone}" /></td>
				<td><form:errors path="softpath_phone" cssClass="error" /></td>
			</tr>

					<tr>
						<td>Username:</td>
						<td><form:input path="username"
								value="${programmer_model.username}" /></td>
						<td><form:errors path="username" cssClass="error" /></td>
					</tr>
			<tr>
				<td>Password:</td>
				<td><form:input path="password"
						value="${programmer_model.password}" /></td>
				<td><form:errors path="password" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Compañia:</td>
				<td><form:input path="company"
						value="${programmer_model.company}" /></td>
				<td><form:errors path="company" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Grupo:</td>
				<td><form:select path="groupNum">
						 <c:forEach var="i" begin="1" end="10">
						 <form:option value="${i}"></form:option>
						 </c:forEach>
						 </form:select></td>
				<td><form:errors path="groupNum" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Dia de contratación:</td>
				<td><form:input path="date_hired" type="date"
						value="${programmer_model.date_hired}" /></td>
				<td><form:errors path="date_hired" cssClass="error" /></td>
			</tr>

			<tr>
				<td>Lugar</td>
				<td><form:input path="place" value="${programmer_model.place}" /></td>
				<td><form:errors path="place" cssClass="error" /></td>

			</tr>

			<tr>
				<td>Salario</td>
				<td><form:input path="salary"
						value="${programmer_model.salary}" /></td>
				<td><form:errors path="salary" cssClass="error" /></td>

			</tr>

			<tr>
						<td>Cantidad que debe</td>
						<td><form:input path="amount_to_pay"
								value="${programmer_model.amount_to_pay}" name="amount_to_pay" /></td>
						<td><form:errors path="amount_to_pay" cssClass="error" /></td>
			</tr>
			<tr>

						<td><input type="submit" value="Crear Programador"
							class="btn btn-success btn-primary btn-lg"></td>
				</tr>
		</table>
		<br>

	</form:form>


</body>
</html>