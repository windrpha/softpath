<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pagos Softpath</title>
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<c:url value="/resources/images/icons/apple-icon-57x57.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/images/icons/apple-icon-114x114.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/images/icons/apple-icon-72x72.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/images/icons/apple-touch-icon-144x144.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<c:url value="/resources/images/icons/apple-touch-icon-60x60.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<c:url value="/resources/images/icons/apple-touch-icon-120x120.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<c:url value="/resources/images/icons/apple-touch-icon-76x76.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<c:url value="/resources/images/icons/apple-touch-icon-152x152.png" />" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-196x196.png" />" sizes="196x196" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-96x96.png" />" sizes="96x96" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-32x32.png" />"  sizes="32x32" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-16x16.png" />" sizes="16x16" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-128.png" />"  sizes="128x128" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="<c:url value="/resources/css/pagos.css" />" rel="stylesheet">
<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o), m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js',
			'ga');

	ga('create', 'UA-75794966-1', 'auto');
	ga('send', 'pageview');
</script>

<style type="text/css">
body {
	background-color: white;
}
</style>

</head>
<body>
	<header>
	  	<div id="softimage"><img 
			src="<c:url value="/resources/images/tecnologies/softpath.jpg" />"
			alt="softpath" width = "100px" height="70px" align="left">
		</div>
		<h4>
			Hola <strong>${programmer_name}</strong>, este es tu desgloce de
			pagos recibidos hasta el momento 
			 <a
				href="<c:url value="/logout" />">Logout</a>
		</h4>
	</header>

	<section>

		<h3>Desgloce de pagos</h3>
		<table>
			<thead>
				<tr>
					<th>Numero de pago</th>
					<th>Fecha de Pago</th>
					<th>Cantidad</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${payments}" var="payment" varStatus="status">
					<tr>
						<td>${status.count}</td>
						<td>${payment[1]}</td>
						<td>${payment[0]}</td>
					</tr>
					<c:set var="suma" value="${suma=payment[0]+suma}"></c:set>
				</c:forEach>
			</tbody>
			<tr>
				<td></td>
				<td><strong>Pagado: </strong></td>
				<td><strong>${suma}</strong></td>
			</tr>
			<tr>
				<td></td>
				<td><strong>Total a Pagar: </strong></td>
				<td><strong>${amount_to_pay + suma}</strong></td>
			</tr>
			<tr>
				<td></td>
				<td><strong>Resta: </strong></td>
				<td><strong>${amount_to_pay}</strong></td>
			</tr>
		</table>
		<div id="softimage">
			<img
				src="<c:url value="/resources/images/softpath_icon.jpg" />"
				alt="softpath" style="width: 200px; height: 140px;">
		</div>
	</section>
	<!--  
	<a href="<c:url value="/DownloadPDF" />">Descargar comprobante de Pagos</a>
	-->
</body>
</html>