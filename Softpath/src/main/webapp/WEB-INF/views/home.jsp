<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<c:url value="/resources/images/icons/apple-icon-57x57.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<c:url value="/resources/images/icons/apple-icon-114x114.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<c:url value="/resources/images/icons/apple-icon-72x72.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<c:url value="/resources/images/icons/apple-touch-icon-144x144.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<c:url value="/resources/images/icons/apple-touch-icon-60x60.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<c:url value="/resources/images/icons/apple-touch-icon-120x120.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<c:url value="/resources/images/icons/apple-touch-icon-76x76.png" />" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<c:url value="/resources/images/icons/apple-touch-icon-152x152.png" />" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-196x196.png" />" sizes="196x196" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-96x96.png" />" sizes="96x96" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-32x32.png" />"  sizes="32x32" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-16x16.png" />" sizes="16x16" />
<link rel="icon" type="image/png" href="<c:url value="/resources/images/icons/favicon-128.png" />"  sizes="128x128" />



<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Softpath</title>

<!-- Bootstrap Core CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
	rel='stylesheet' type='text/css'>
<link href="<c:url value="/resources/css/font-awesome.min.css" />"
	type="text/css" rel="stylesheet">

<!-- Plugin CSS -->
<link href="<c:url value="/resources/css/animate.min.css" />"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<c:url value="/resources/css/creative.css" />"
	rel="stylesheet">

<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o), m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js',
			'ga');

	ga('create', 'UA-75794966-1', 'auto');
	ga('send', 'pageview');
</script>


<style type="text/css">
#liexperiencia {
	float: left;
}
</style>

</head>

<body id="page-top">


	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand page-scroll" href="#page-top"></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="page-scroll" href="#about">Acerca de
							nosotros</a></li>
					<li><a class="page-scroll" href="#services">Capacitación</a></li>
					<li><a class="page-scroll" href="#portfolio">Egresados</a></li>
					<li><a class="page-scroll" href="#contact">Contacto</a></li>
					<li><a class="page-scroll" href="login">Login</a></li>
				</ul>


			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

	<header>
		<div class="header-content">
			<div class="header-content-inner">
				<p class="text-faded">¿Buscas mejorar tus conocimientos en
					programación con el fin de encontrar un trabajo con ingresos
					altamente competitivos? Si es así, ¡SoftPath es tu mejor opción!.
					Nos comprometemos a formarte como un desarrollador profesional en
					un tiempo verdaderamente corto. ¿Qué esperas? únete a SoftPath.</p>
				<hr>
				<p>SoftPath, Empower your success...</p>
				<a href="#about" class="btn btn-primary btn-xl page-scroll">¿Quienes
					somos?</a>
			</div>
		</div>
	</header>

	<section class="bg-primary" id="about">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 text-center">
					<h2 class="section-heading">¿Quienes somos?</h2>
					<hr class="light">
					<p class="text-faded">Somos una empresa de capacitación y
						posicionamiento laboral. Ofrecemos a los profesionistas los más
						sofisticados conocimientos y herramientas del lenguaje de
						programación Java, en un periodo verdaderamente corto. Así como
						apoyarlos en el proceso de reclutamiento, dando como resultado
						final su colocación en las empresas de TI más reconocidas en el
						mercado nacional e internacional, obteniendo salarios altamente
						competitivos.</p>
					<a href="#contact" class="btn btn-default btn-xl page-scroll">¡Contáctanos!</a>
				</div>
			</div>
		</div>
	</section>

	<section id="services">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">Ponemos a tu disposición</h2>
					<hr class="primary">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-diamond wow bounceIn text-primary"></i>
						<h3>Contenido</h3>
						<p class="text-muted">Contenido del más alto nivel en
							tecnologias Java.</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-paper-plane wow bounceIn text-primary"
							data-wow-delay=".1s"></i>
						<h3>Tiempo</h3>
						<p class="text-muted">Te capacitamos en un tiempo
							verdaderamente corto</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-newspaper-o wow bounceIn text-primary"
							data-wow-delay=".2s"></i>
						<h3>Actualización</h3>
						<p class="text-muted">Conoce las tecnologias más nuevas usadas
							en el mercado laboral</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-heart wow bounceIn text-primary"
							data-wow-delay=".3s"></i>
						<h3>Exito</h3>
						<p class="text-muted">Conviértete en un profesional altamente
							competitivo</p>
					</div>
				</div>
			</div>
		</div>
	</section>



	<aside class="bg-dark">
		<div class="container text-center">
			<div class="call-to-action">
				<h2>¡Orgullosamente Egresados Softpath!</h2>
				<!--        <a href="#" class="btn btn-default btn-xl wow tada">Download Now!</a>-->
			</div>
		</div>
	</aside>

	<section class="no-padding" id="portfolio">
		<div class="container-fluid">
			<div class="row no-gutter">
				<div class="col-lg-4 col-sm-6 col-lg-offset-2">
					<a href="#" class="portfolio-box"> <img
						src="<c:url value="/resources/images/capacitacion/enero-febrero.png" />"
						class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">Generación 1</div>
								<div class="project-name">Enero - Febrero</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4">
					<a href="#" class="portfolio-box"> <img
						src="<c:url value="/resources/images/capacitacion/febrero-marzo.png" />"
						class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">Generación 2</div>
								<div class="project-name">Febrero - Marzo</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-lg-offset-2">
					<a href="#" class="portfolio-box"> <img
						src="<c:url value="/resources/images/capacitacion/marzo-abril.png" />"
						class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">Generación 3</div>
								<div class="project-name">Marzo-Abril</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4">
					<a href="#" class="portfolio-box"> <img
						src="<c:url value="/resources/images/capacitacion/junio-julio.png" />"
						class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">Generación 4</div>
								<div class="project-name">Junio-Julio</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-lg-offset-2">
					<a href="#" class="portfolio-box"> <img
						src="<c:url value="/resources/images/capacitacion/interrogation.png" />"
						class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">Generación 5</div>
								<div class="project-name">Agosto-Septiembre</div>
							</div>
						</div>
					</a>
				</div>

				<aside class="bg-dark">
					<div class="container text-center">
						<div class="call-to-action">
							<!--        <a href="#" class="btn btn-default btn-xl wow tada">Download Now!</a>-->
						</div>
					</div>
				</aside>


				<div class="col-lg-2 col-lg-offset-2">
					<a href="http://www.oracle.com/technetwork/java/index.html"
						target="_blank" class="portfolio-box"> <img
						src="<c:url value="/resources/images/tecnologies/Java.png" />"
						class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">Java SE</div>
								<div class="project-name">Version 7</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-2">
					<a href="http://hibernate.org/" class="portfolio-box"
						target="_blank"> <img
						src="<c:url value="/resources/images/tecnologies/hibernate.png" />"
						class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">Hibernate</div>
								<div class="project-name">con JPA</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-2">
					<a href="https://spring.io/" target="_blank" class="portfolio-box">
						<img
						src="<c:url value="/resources/images/tecnologies/spring.png" />"
						class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">Spring framework</div>
								<div class="project-name">AOP,MVC,RESTful</div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-2">
					<a href="https://angularjs.org/" target="_blank"
						class="portfolio-box"> <img
						src="<c:url value="/resources/images/tecnologies/angularjs.png" />"
						class="img-responsive" alt="">
						<div class="portfolio-box-caption">
							<div class="portfolio-box-caption-content">
								<div class="project-category text-faded">AngularJS</div>
								<div class="project-name">Angular Framework</div>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>



	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 text-center">
					<h2 class="section-heading">¡Contáctanos!</h2>
					<hr class="primary">
					<p>¿Estás listo para convertirte en un profesional altamente
						calificado y preparado para el mundo laboral? ... ¡Tú tomas la
						decisión!</p>
				</div>
				<div class="col-lg-3 col-lg-offset-2 text-center">
					<i class="fa fa-phone fa-3x wow bounceIn"></i>
					<p>33 3502 3353</p>
				</div>
				<div class="col-lg-2 text-center">
					<i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
					<p>
						<a href="mailto:your-email@your-domain.com">jobs@softpathtec.com</a>
					</p>
				</div>
				<div class="col-lg-3 text-center">
					<i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
					<p>
						<a href="https://www.facebook.com/SoftPathmx/" target="_blank">Facebook</a>
					</p>
				</div>
			</div>
		</div>
	</section>


	<!-- jQuery -->
	<script src="<c:url value="/resources/js/jquery.js" />"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

	<!-- Plugin JavaScript -->
	<script src="<c:url value="/resources/js/jquery.easing.min.js" />"></script>
	<script src="<c:url value="/resources/js/jquery.fittext.js" />"></script>
	<script src="<c:url value="/resources/js/wow.min.js" />"></script>


	<!-- Custom Theme JavaScript -->
	<script src="<c:url value="/resources/js/creative.js" />"></script>


</body>

</html>
